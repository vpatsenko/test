## Test assignment

The idea is simple. Client gets chunks of data through http calls, frontend remembers current chunk and increments it for the next request.

## Trying out
Just clone the repository. And run. 

```
make all
```
This command will run docker-compose in background and build all the services and will apply the migration. There is a hardcoded delay inside Makefile to make sure that postgres is ready to accept connections. If migration didn't worked out just stop containers, remove them and increase delay inside the Makefile.


I didn't have enough time to code frontend and metrics so it could be tested out using curl.
```
curl http://localhost:8081/0 #will return first chunk of data and so on
```
