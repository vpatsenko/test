create table usage
(
    id               bigserial primary key,
    meter_usage_date timestamp not null,
    value            float4    not null
);

COPY usage(meter_usage_date, value)
FROM '/data_source/meterusage.csv'
DELIMITER ',' CSV HEADER;