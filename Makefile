
.PHONY: all
all: compose migrate

.PHONY: compose
compose:
	docker-compose up -d
	sleep 30s

.PHONY: migrate
migrate:
	go get github.com/golang-migrate/migrate@latest
	migrate -path migration -database "postgresql://gopher:gopher@localhost:5432/postgres?sslmode=disable" -verbose up

