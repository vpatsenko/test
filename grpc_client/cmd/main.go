package main

import (
	"client/internal/config"
	"client/internal/grpc_client"
	"client/internal/server"
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {

	cfg, err := config.NewConfig("/app")
	if err != nil {
		log.Fatalln("failed to create config", err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	grpClient, err := grpc_client.NewGrpcClient(ctx, &cfg.Grpc)
	if err != nil {
		log.Fatalln("failed to create grpc client", err)
	}

	httpServer, err := server.NewServer(cfg.Http, grpClient)
	if err != nil {
		log.Fatalln("failed to create http server", err)
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
		syscall.SIGHUP,
	)

	go func() {
		for {
			select {
			case <-done:
				log.Println("received shutdown signal")
				cancel()
				return
			default:
			}
		}
	}()

	if err := httpServer.Run(ctx); err != nil {
		log.Fatalln("failed to run http server")
	}
}
