package config

import (
	"client/internal/grpc_client"
	"client/internal/server"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

type Config struct {
	Grpc grpc_client.Config `mapstructure:"grpc"`
	Http server.Config      `mapstructure:"http"`
}

func NewConfig(configPath string) (*Config, error) {
	src := viper.New()
	src.SetConfigName("config")
	src.SetConfigType("yaml")
	src.AddConfigPath(configPath)

	if err := src.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "read in config")
	}

	httpCfg := server.Config{}
	if err := src.Sub("http").Unmarshal(&httpCfg); err != nil {
		return nil, errors.Wrap(err, "unmarshal config")
	}

	grpcCfg := grpc_client.Config{}
	if err := src.Sub("grpc").Unmarshal(&grpcCfg); err != nil {
		return nil, errors.Wrap(err, "unmarshal config")
	}

	return &Config{
		Grpc: grpcCfg,
		Http: httpCfg,
	}, nil
}
