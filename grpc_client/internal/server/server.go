package server

import (
	"client/internal/grpc_client"
	"context"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"log"
	"strconv"

	"github.com/gorilla/mux"
	"net/http"
)

type Server struct {
	grpc grpc_client.GrpcClient
	srv  *http.Server
}

func NewServer(cfg Config, svc grpc_client.GrpcClient) (Server, error) {

	r := mux.NewRouter()

	srv := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", cfg.HttpHost, cfg.HttpPort),
		Handler: r,
	}

	s := Server{
		grpc: svc,
		srv:  srv,
	}

	r.HandleFunc("/{offset}", s.getChunk).Methods(http.MethodGet)

	return s, nil
}

func (s *Server) Run(ctx context.Context) error {
	go func() {
		for {
			select {
			case <-ctx.Done():
				log.Println("shutting down the server ...")
				s.srv.Shutdown(ctx)
				return
			default:
			}
		}
	}()

	log.Println(fmt.Sprintf("starting http server on %v", s.srv.Addr))
	if err := s.srv.ListenAndServe(); err != nil {
		if err != http.ErrServerClosed {
			return nil
		}
		return errors.Wrap(err, "failed to listen http")
	}

	return nil
}

func (s *Server) getChunk(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	offsetStr, ok := vars["offset"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	offset, err := strconv.Atoi(offsetStr)
	if err != nil {
		log.Println("failed to convert offset string to int, offset", offsetStr, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	usageSlice, err := s.grpc.GetChunk(r.Context(), offset)
	if err != nil {
		log.Println("failed to get chunk", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	chunk := make([]Usage, 0, len(usageSlice))
	for _, u := range usageSlice {
		chunk = append(chunk, Usage{
			Date:  u.Date,
			Usage: u.Usage,
		})
	}

	resp := GetChunkResponse{Chunk: chunk}
	payload, err := json.Marshal(resp)
	if err != nil {
		log.Println("failed to marshall payload", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	if _, err := w.Write(payload); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
