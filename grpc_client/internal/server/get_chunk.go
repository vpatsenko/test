package server

type GetChunkResponse struct {
	Chunk []Usage `json:"chunk"`
}

type Usage struct {
	Date  string  `json:"date"`
	Usage float32 `json:"usage"`
}
