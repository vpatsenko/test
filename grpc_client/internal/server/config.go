package server

type Config struct {
	HttpPort int    `mapstructure:"port"`
	HttpHost string `mapstructure:"host"`
}
