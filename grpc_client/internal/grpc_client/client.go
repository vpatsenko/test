package grpc_client

import (
	"client/api"
	"context"
	"fmt"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

type GrpcClient interface {
	GetChunk(ctx context.Context, offset int) ([]*api.Usage, error)
	Close() error
}

type grpcClient struct {
	cl   api.UsageServiceClient
	conn *grpc.ClientConn
}

func NewGrpcClient(ctx context.Context, cfg *Config) (GrpcClient, error) {

	conn, err := grpc.DialContext(ctx, fmt.Sprintf("%v:%d", cfg.GrpcHost, cfg.GrpcPort), grpc.WithInsecure())
	if err != nil {
		return nil, errors.Wrap(err, "failed to dial")
	}

	usageServiceClient := api.NewUsageServiceClient(conn)
	return &grpcClient{
		cl: usageServiceClient,
	}, nil
}

func (gc *grpcClient) GetChunk(ctx context.Context, offset int) ([]*api.Usage, error) {

	resp, err := gc.cl.GetChunk(ctx, &api.GetChunkRequest{Offset: int64(offset)})
	if err != nil {
		return nil, errors.Wrap(err, "failed go get chunk")
	}

	if resp.Result != api.Result_OK {
		return nil, errors.New(resp.Error.GetMessage())
	}

	return resp.Payload.GetUsageChunk(), nil
}

func (gc *grpcClient) Close() error {
	return gc.conn.Close()
}
