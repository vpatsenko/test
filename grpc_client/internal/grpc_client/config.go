package grpc_client

type Config struct {
	GrpcPort int    `mapstructure:"port"`
	GrpcHost string `mapstructure:"host"`
}
