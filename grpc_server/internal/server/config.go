package server

type ServerConfig struct{
	Host         string            `mapstructure:"host"`
	Port         int16             `mapstructure:"port"`
}
