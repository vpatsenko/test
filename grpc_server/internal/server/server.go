package server

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"log"
	"net"
	"server/api"
	"server/internal/service"
	"strconv"
)

type GrpcServer struct {
	svc     service.Service
	port    int16
	gRpcSrv *grpc.Server

	api.UnimplementedUsageServiceServer
}

func NewGrpcServer(cfg ServerConfig, svc service.Service) (*GrpcServer, error) {

	s := grpc.NewServer()

	srv := &GrpcServer{
		svc:     svc,
		port:    cfg.Port,
		gRpcSrv: s,
	}

	api.RegisterUsageServiceServer(s, srv)
	return srv, nil
}

func (gs *GrpcServer) Run(ctx context.Context) error {
	lis, err := net.Listen("tcp", ":"+strconv.Itoa(int(gs.port)))
	if err != nil {
		return errors.Wrap(err, "listen port "+strconv.Itoa(int(gs.port)))
	}

	go func() {
		for {
			select {
			case <-ctx.Done():
				log.Println("shutting down grpc service ...")
				gs.gRpcSrv.GracefulStop()
				gs.svc.Close(ctx)
				return
			default:
			}
		}
	}()

	log.Println(fmt.Sprintf("grpc server listens on port %v", lis.Addr()))
	err = gs.gRpcSrv.Serve(lis)
	if err != nil {
		return errors.Wrap(err, "serve grpc")
	}

	return nil
}

func (gs *GrpcServer) GetChunk(ctx context.Context, req *api.GetChunkRequest) (*api.GetChunkResponse, error) {
	payload, err := gs.svc.GetChunk(ctx, int(req.Offset))
	if err != nil {
		return &api.GetChunkResponse{
			Result: api.Result_ERROR,
			Error: &api.Error{
				Message: err.Error(),
			},
		}, nil
	}
	return &api.GetChunkResponse{
		Result: api.Result_OK,
		Payload: &api.GetChunkPayload{
			UsageChunk: payload,
		},
	}, nil
}
