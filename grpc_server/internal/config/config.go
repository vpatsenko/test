package config

import (
	"server/internal/repository"
	"server/internal/server"
	"server/internal/service"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

type Config struct {
	Grpc    server.ServerConfig `mapstructure:"grpc"`
	Db      repository.Config   `mapstructure:"db"`
	Service service.Config      `mapstructure:"service"`
}

func NewConfig(configPath string) (*Config, error) {
	src := viper.New()
	src.SetConfigName("config")
	src.SetConfigType("yaml")
	src.AddConfigPath(configPath)

	if err := src.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "read in config")
	}

	cfg := &Config{}
	if err := src.Unmarshal(cfg); err != nil {
		return nil, errors.Wrap(err, "unmarshal config")
	}
	return cfg, nil
}
