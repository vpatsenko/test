package service

import (
	"context"
	"server/api"
	"server/internal/repository"
	"time"
)

type Service interface {
	GetChunk(ctx context.Context, offset int) ([]*api.Usage, error)
	Close(ctx context.Context) error
}

type service struct {
	repo      repository.Repository
	chunkSize int
}

func NewService(cfg *Config, repo repository.Repository) Service {
	return &service{
		repo:      repo,
		chunkSize: cfg.ChunkSize,
	}
}

func (s *service) GetChunk(ctx context.Context, offset int) ([]*api.Usage, error) {
	usageSlice, err := s.repo.Get(ctx, s.chunkSize, offset)
	if err != nil {
		return nil, err
	}

	payload := make([]*api.Usage, 0, len(usageSlice))
	for _, u := range usageSlice {
		payload = append(payload, &api.Usage{
			Date:  u.Date.Format(time.RFC1123),
			Usage: u.MeterUsage,
		})
	}

	return payload, nil
}

func (s *service) Close(ctx context.Context) error {
	return s.repo.Close(ctx)
}
