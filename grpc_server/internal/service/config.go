package service

type Config struct {
	ChunkSize int `mapstructure:"chunk-size"`
}
