package repository

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4"
	"github.com/pkg/errors"
	"log"
	"server/internal/repository/usage"
	"time"
)

type Repository interface {
	Get(ctx context.Context, chunkSize, offset int) ([]*usage.Usage, error)
	Close(ctx context.Context) error
}

type repository struct {
	conn *pgx.Conn
}

func NewRepository(ctx context.Context, cfg Config) (Repository, error) {
	url := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		cfg.Host, cfg.Port, cfg.UserName, cfg.Password, cfg.DBname)

	ctx, cancel := context.WithTimeout(ctx, time.Second*3)
	defer cancel()

	conn, err := pgx.Connect(ctx, url)
	if err != nil {
		return nil, err
	}

	if err := conn.Ping(ctx); err != nil {
		return nil, errors.Wrap(err, "failed to ping db")
	}

	return &repository{
		conn: conn,
	}, nil
}

const selectWithOffset = `
SELECT meter_usage_date, value FROM
usage 
OFFSET $1 LIMIT $2`

func (r *repository) Get(ctx context.Context, chunkSize, offset int) ([]*usage.Usage, error) {

	rows, err := r.conn.Query(ctx, selectWithOffset, offset * chunkSize, chunkSize)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	usageChunk := make([]*usage.Usage, 0, chunkSize)
	//u := new(usage.Usage)
	//if err := rows.Scan(&u.Date, &u.MeterUsage); err != nil {
	//	return nil, err
	//}

	for rows.Next() {
		u := new(usage.Usage)
		if err := rows.Scan(&u.Date, &u.MeterUsage); err != nil {
			return nil, err
		}

		usageChunk = append(usageChunk, u)
	}

	return usageChunk, nil
}
func (r *repository) Close(ctx context.Context) error {
	log.Println("closing database...")
	return r.conn.Close(ctx)
}
