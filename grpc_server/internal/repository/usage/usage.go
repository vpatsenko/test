package usage

import "time"

type Usage struct{
	Date time.Time
	MeterUsage float32
}
