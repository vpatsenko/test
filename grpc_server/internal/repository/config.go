package repository

type Config struct {
	Host     string `mapstructure:"host"`
	Port     int16  `mapstructure:"port"`
	UserName string `mapstructure:"username"`
	Password string `mapstructurer:"password"`
	DBname   string `mapstructurer:"dbname"`
}
