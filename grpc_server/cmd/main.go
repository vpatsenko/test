package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"server/internal/config"
	"server/internal/repository"
	"server/internal/server"
	"server/internal/service"
	"syscall"
)

func main() {

	cfg, err := config.NewConfig("/app")
	if err != nil {
		log.Fatalln(err)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	repo, err := repository.NewRepository(ctx, cfg.Db)
	if err != nil {
		log.Fatalln("failed to initialize repository", err)
	}

	svc := service.NewService(&cfg.Service, repo)

	srv, err := server.NewGrpcServer(cfg.Grpc, svc)
	if err != nil {
		log.Fatalln("failed to create grpc server", err)
	}

	done := make(chan os.Signal, 1)
	signal.Notify(done,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
		syscall.SIGHUP,
	)

	go func(){
		for{
			select{
			case <- done:
				log.Println("received shutdown signal")
				cancel()
				return
			default:
			}
		}
	}()

	if err = srv.Run(ctx); err != nil {
		log.Fatalln("failed to run grpc server", err)
	}
}
